package softwareprojekt.dtu.com.findmysupervisor;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class MatchButtonsFragment extends Fragment implements View.OnClickListener{


    private OnMatchButtonsListener mListener;

    public interface OnMatchButtonsListener {
        // TODO: Update argument type and name
        void onAcceptPressed();
        void onDeclinePressed();
    }

    public static MatchButtonsFragment newInstance() {
        MatchButtonsFragment fragment = new MatchButtonsFragment();

        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public void setOnMatchButtonsListener(OnMatchButtonsListener listener) {
        this.mListener = listener;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View mainView = inflater.inflate(R.layout.fragment_match_buttons, container,false);

        FloatingActionButton btnLike = mainView.findViewById(R.id.btnLike);
        btnLike.setOnClickListener(this);

        FloatingActionButton btnNope = mainView.findViewById(R.id.btnNope);
        btnNope.setOnClickListener(this);

        return mainView;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.btnLike:
                if (mListener != null) {
                    mListener.onAcceptPressed();
                }
                break;
            case R.id.btnNope:
                if (mListener != null) {
                    mListener.onDeclinePressed();
                }
                break;
        }
    }
}
