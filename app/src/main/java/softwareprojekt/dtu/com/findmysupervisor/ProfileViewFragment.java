package softwareprojekt.dtu.com.findmysupervisor;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.nex3z.flowlayout.FlowLayout;

import java.io.ByteArrayOutputStream;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import static android.app.Activity.RESULT_OK;


public class ProfileViewFragment extends Fragment {

    public static final String TAG = "ProfileView";

    FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseStorage storage = FirebaseStorage.getInstance();

    DatabaseReference userRef = database.getReference(currentUser.getUid());

    StorageReference storageRef = storage.getReference();
    StorageReference pictureRef = storageRef.child("images/" + mAuth.getCurrentUser().getUid());

    UploadTask uploadPictureTask;
    static Matching matching;

    public static final int PICK_IMAGE = 1;
    public static final int REQUEST_READ_PHOTO = 2;

    String picturePath;
    View profileView;
    ImageView ivProfilePhoto;
    Bitmap scaledProfile;

    public ProfileViewFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static ProfileViewFragment newInstance(Matching currentMatching) {
        ProfileViewFragment fragment = new ProfileViewFragment();
        matching = currentMatching;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        profileView = inflater.inflate(R.layout.fragment_profile, container,false);
        Button btnSaveProfileChanges = profileView.findViewById(R.id.saveButton);
        Button btnLogOut = profileView.findViewById(R.id.logOutButton);

        final EditText name = profileView.findViewById(R.id.tvProfileFullName);
        final EditText bio = profileView.findViewById(R.id.tvProfileBio);

        final Spinner spStudyProgramme = profileView.findViewById(R.id.spStudyProgramme);
        final ArrayAdapter<String> spAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, getResources().getStringArray(R.array.studyProgrammes));
        spStudyProgramme.setAdapter(spAdapter);

        final Spinner spProjectPeriod = profileView.findViewById(R.id.spProjectPeriod);
        final ArrayAdapter<String> ppAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, getFutureProjectPeriods());
        spProjectPeriod.setAdapter(ppAdapter);

        final EditText etHighlightedCourse, etProjectTag;
        ImageButton ibAddCourse, ibAddProjectTag;

        FlowLayout highlightedCourses = profileView.findViewById(R.id.flHighlightedCourse);
        final FlowBubbleManager courseBubbleManager = new FlowBubbleManager(getActivity(), highlightedCourses);

        etHighlightedCourse = profileView.findViewById(R.id.etAddHighlightedCourse);
        etHighlightedCourse.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((actionId & EditorInfo.IME_MASK_ACTION) != 0) {
                    String courseName = etHighlightedCourse.getText().toString();
                    if (courseName.length() > 0) {
                        courseBubbleManager.add(courseName);
                        etHighlightedCourse.setText("");
                    }
                    return true;
                }
                else {
                    return false;
                }
            }
        });


        ibAddCourse = profileView.findViewById(R.id.ibAddHighlightedCourse);
        ibAddCourse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String courseName = etHighlightedCourse.getText().toString();
                if (courseName.length() > 0) {
                    courseBubbleManager.add(courseName);
                    etHighlightedCourse.setText("");
                }
            }
        });

        FlowLayout projectTags = profileView.findViewById(R.id.flProjectTags);
        final FlowBubbleManager tagBubbleManager = new FlowBubbleManager(getActivity(), projectTags);


        etProjectTag = profileView.findViewById(R.id.etAddProjectTag);
        etProjectTag.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((actionId & EditorInfo.IME_MASK_ACTION) != 0) {
                    String projectTag = etProjectTag.getText().toString();
                    if (projectTag.length() > 0) {
                        tagBubbleManager.add(projectTag);
                        etProjectTag.setText("");
                    }
                    return true;
                }
                else {
                    return false;
                }
            }
        });

        ibAddProjectTag = profileView.findViewById(R.id.ibAddProjectTag);
        ibAddProjectTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String projectTag = etProjectTag.getText().toString();
                if (projectTag.length() > 0) {
                    tagBubbleManager.add(projectTag);
                    etProjectTag.setText("");
                }
            }
        });


        CardView btnSelectPicture = profileView.findViewById(R.id.cvRoundProfile);
        btnSelectPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pictureFromGallery();
            }
        });

        ivProfilePhoto = profileView.findViewById(R.id.ivProfilePicture);

        userRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Iterable<DataSnapshot> dataEntries = dataSnapshot.getChildren();
                pictureRef.getBytes(Long.MAX_VALUE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                    @Override
                    public void onSuccess(byte[] bytes) {
                        Bitmap bitmap = BitmapFactory.decodeByteArray(bytes,0, bytes.length);
                        if(ivProfilePhoto != null) {
                            ivProfilePhoto.setImageBitmap(bitmap);
                        }
                    }
                });

                for (DataSnapshot data: dataEntries){
                    //Log.i("DATASNAPSHOT", data.getKey().toString());
                    if(data.getKey().equals("Name")){
                        name.setText(dataSnapshot.child("Name").getValue().toString());
                    }
                    if(data.getKey().equals("Bio")){
                        bio.setText(dataSnapshot.child("Bio").getValue().toString());
                    }
                    if(data.getKey().equals("ProjectPeriod")){
                        int pos = ppAdapter.getPosition(dataSnapshot.child("ProjectPeriod").getValue().toString());
                        spProjectPeriod.setSelection(pos);
                    }
                    if(data.getKey().equals("StudyProgramme")){
                        int pos = spAdapter.getPosition(dataSnapshot.child("StudyProgramme").getValue().toString());
                        spStudyProgramme.setSelection(pos);
                    }
                    if(data.getKey().equals("Courses")){
                        List<String >stringCourses = (List<String>) dataSnapshot.child("Courses").getValue();
                        for (String coursename: stringCourses) {
                            courseBubbleManager.add(coursename);
                        }
                    }
                    if(data.getKey().equals("Tags")){
                        List<String > stringTags = (List<String>) dataSnapshot.child("Tags").getValue();
                        for (String tag: stringTags) {
                            tagBubbleManager.add(tag);
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.w("FIREBASE_FAILURE", "Failed to read value.", databaseError.toException());

            }
        });

        btnLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAuth.signOut();
                Intent backToLoginScreen = new Intent(getContext(), LoginActivity.class);

                startActivity(backToLoginScreen);
                getActivity().finish();
            }
        });

        btnSaveProfileChanges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String studentName = name.getText().toString();
                String studentBio = bio.getText().toString();
                String programme = spStudyProgramme.getSelectedItem().toString();
                String period =  spProjectPeriod.getSelectedItem().toString();

                // Updates all fields in a single Firebase-call
                HashMap<String, Object> dataToBeSaved = new HashMap<String, Object>();
                dataToBeSaved.put("Name", studentName);
                dataToBeSaved.put("Bio", studentBio);
                dataToBeSaved.put("StudyProgramme", programme);
                dataToBeSaved.put("ProjectPeriod", period);
                dataToBeSaved.put("Courses", courseBubbleManager.getElements());
                dataToBeSaved.put("Tags", tagBubbleManager.getElements());
                if(picturePath != null){
                    uploadImage();
                }


                userRef.updateChildren(dataToBeSaved, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                        Toast.makeText(getActivity(),
                                getResources().getString(R.string.changes_saved),
                                Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        return profileView;
    }

    public void setProfilePhoto(String uri) {
        Bitmap profileImage = BitmapFactory.decodeFile(uri);
        Log.i(TAG, "Width: " + profileImage.getWidth() + " Height: " + profileImage.getHeight());
        double ratio = 1.0 * profileImage.getHeight() / profileImage.getWidth();
        scaledProfile = Bitmap.createScaledBitmap(profileImage, 200, (int)(200 * ratio), false);
        if(ivProfilePhoto != null) {
            ivProfilePhoto.setImageBitmap(scaledProfile);
        }
    }


    private void uploadImage() {
        if(picturePath != null) {
            final ProgressDialog progressDialog = new ProgressDialog(getActivity());
            progressDialog.setTitle("Uploading...");
            progressDialog.show();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            scaledProfile.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] data = baos.toByteArray();

            StorageReference ref = storageRef.child("images/"+ mAuth.getCurrentUser().getUid());

            uploadPictureTask = ref.putBytes(data);

            uploadPictureTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressDialog.dismiss();

                    Toast.makeText(getActivity(),
                            getResources().getString(R.string.picture_upload_failed),
                            Toast.LENGTH_SHORT).show();
                }
            });

            uploadPictureTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    progressDialog.dismiss();
                }
            });
        }
    }

    public String[] getFutureProjectPeriods() {

        Calendar currentTime = Calendar.getInstance();
        Integer month = currentTime.get(Calendar.MONTH);
        boolean startSpring = (month < Calendar.MARCH || month > Calendar.SEPTEMBER);
        int startYear = currentTime.get(Calendar.YEAR);
        startYear = (month > Calendar.SEPTEMBER) ? startYear + 1 : startYear;
        String[] periods = new String[6];

        for (int i = 0; i < periods.length; i++) {
            periods[i] = (startSpring) ? "Spring " + startYear : "Fall " + startYear;
            // If the current is a fall period, year should be increment for next run
            if (!startSpring) {
                startYear++;
            }
            // Flip spring/fall
            startSpring = !startSpring;
        }
        return periods;
    }

    public void pictureFromGallery(){
        int permissionCheck = getActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_READ_PHOTO);
        } else {
            startGallery();
        }
    }

    public void startGallery(){
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, PICK_IMAGE);
    }


    // https://stackoverflow.com/questions/10473823/android-get-image-from-gallery-into-imageview
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK) {
            Uri image = data.getData();

            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getActivity().getContentResolver().query(image, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();

            setProfilePhoto(picturePath);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == ProfileViewFragment.REQUEST_READ_PHOTO) {
                startGallery();
            }
        }
    }
}
