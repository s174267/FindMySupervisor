package softwareprojekt.dtu.com.findmysupervisor;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Matching {

    public interface MatchingListener {
        void onNewUsersFetched();
    }

    public Matching(FirebaseUser user){
        this.currentUser = user;
        getUsers();

    }

    FirebaseUser currentUser;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference userList = database.getReference();

    List<User> users = new ArrayList<>();
    List<User> sortedUsers = new ArrayList<>();
    List<String> matches = new ArrayList<>();

    public static MatchingListener mListener;

    public static void setOnNewUsersFetchedListener(MatchingListener listener) {
        mListener = listener;
    }

    public List<User> matchUsers(){
        User loggedInUser = findUserByID(currentUser.getUid());
        HashMap<User, Double> userScores = new HashMap<>();
        Log.i("MATCHINGDEBUG", "MATCHING STARTED");
        for (User potentialMatch: users) {
            try {
                Double score = 0.00;
                if (loggedInUser.studyProgramme.equals(potentialMatch.studyProgramme)) {
                    score = score + 5;
                }

                for (String tag : potentialMatch.tags) {
                    if (loggedInUser.tags.contains(tag.toLowerCase())) {
                        score = score + 1;
                    }
                }
                for (String course : potentialMatch.courses) {
                    if (loggedInUser.courses.contains(course.toLowerCase())) {
                        score = score + 0.5;
                    }
                }
                Log.i("LOGGEDINUSER User", loggedInUser.UID);
                Log.i("LOGGEDINUSER MATCH", potentialMatch.UID);
                if (loggedInUser.period.equals(potentialMatch.period)) {
                    if (!loggedInUser.declinedUsers.contains(potentialMatch.UID)) {
                        if (!loggedInUser.acceptedUsers.contains(potentialMatch.UID)) {
                            if (!loggedInUser.UID.equals(potentialMatch.UID)) {
                                Log.i("MATCHINGDEBUG UID", potentialMatch.UID.toString());
                                userScores.put(potentialMatch, score);
                            }

                        }
                    }

                }
            }
            catch (Exception e){
                Log.i("NULLPOINTER", "Error");
            }
        }

        return sortHashMapByValues(userScores);
    }

    public User findUserByID(String UID){
        for(User user: users){
            if(user.UID.equals(UID)){
                return user;
            }
        }
        return null;
    }


    public List<User> sortHashMapByValues( HashMap<User, Double> passedMap) {

        List<User> mapKeys = new ArrayList<>(passedMap.keySet());
        List<Double> mapValues = new ArrayList<>(passedMap.values());

        for (int i = 0; i <= mapValues.size()-1; i++ ){
            int j = i;
            while(j > 0 && mapValues.get(j-1) < mapValues.get(j)){
                Double tempD = mapValues.get(j);
                User tempU = mapKeys.get(j);
                mapValues.set(j, mapValues.get(j-1));
                mapValues.set(j-1, tempD);
                mapKeys.set(j, mapKeys.get(j-1));
                mapKeys.set(j-1, tempU);

                j = j-1;
            }
        }
        return mapKeys;
    }


    public void getUsers(){

        userList.addListenerForSingleValueEvent(new ValueEventListener() {
            String name = "";
            String bio = "";
            String studyProgramme ="";
            String period ="";
            List<String> courses;
            List<String> tags;
            List<String> acceptedUsers;
            List<String> acceptedMe;
            List<String> declinedUsers;
            String email;
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                users.removeAll(users);
                Iterable<DataSnapshot> userData = dataSnapshot.getChildren();
                for (DataSnapshot user : userData) {
                    Iterable<DataSnapshot> userInfo = user.getChildren();
                    for (DataSnapshot data : userInfo) {
                        if (data.getKey().equals("Name")) {
                            name = user.child("Name").getValue().toString();

                        } else if (data.getKey().equals("Bio")) {
                            bio = user.child("Bio").getValue().toString();

                        } else if (data.getKey().equals("ProjectPeriod")) {
                            period = user.child("ProjectPeriod").getValue().toString();

                        } else if (data.getKey().equals("StudyProgramme")) {
                            studyProgramme = user.child("StudyProgramme").getValue().toString();


                        } else if (data.getKey().equals("Courses")) {
                            courses = (List<String>) user.child("Courses").getValue();

                        } else if (data.getKey().equals("Tags")) {
                            tags = (List<String>) user.child("Tags").getValue();
                        } else if (data.getKey().equals("Email")) {
                            email = user.child("Email").getValue().toString();

                        }

                    }
                    User newUser = new User(user.getKey(), name, bio, studyProgramme, period, courses, tags, email);
                    if(!newUser.inList(users)){
                        users.add(newUser);
                    }
                }

                Iterable<DataSnapshot> userData2 = dataSnapshot.getChildren();

                for (DataSnapshot user : userData2) {
                    Iterable<DataSnapshot> userInfo = user.getChildren();
                    for (DataSnapshot data : userInfo) {
                        if (data.getKey().equals("AcceptedUsers")) {
                            acceptedUsers = (List<String>) user.child("AcceptedUsers").getValue();
                            findUserByID(user.getKey()).setAcceptedUsers(acceptedUsers);

                        } else if (data.getKey().equals("AcceptedMe")) {
                            acceptedMe = (List<String>) user.child("AcceptedMe").getValue();
                            findUserByID(user.getKey()).setAcceptedMe(acceptedMe);

                        } else if (data.getKey().equals("DeclinedUsers")) {
                            declinedUsers = (List<String>) user.child("DeclinedUsers").getValue();
                            findUserByID(user.getKey()).setDeclinedUsers(declinedUsers);
                        }
                    }
                }
                Log.i("LOGGEDINUSER UID", FirebaseAuth.getInstance().getCurrentUser().getUid());
                sortedUsers = matchUsers();
                if (mListener != null) {
                    mListener.onNewUsersFetched();
                }

                getMatches();
                listenForMatches();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }

        });

    }
   public void getMatches(){
        List<String> matchedUsers = new ArrayList<>();
        User loggedInUser = findUserByID(currentUser.getUid());
       Log.i("NULLUSER SIZE ", Integer.toString(loggedInUser.acceptedUsers.size()));
        for (String accepted: loggedInUser.acceptedUsers){
            if (loggedInUser.acceptedMe.contains(accepted)){
                matchedUsers.add(accepted);
            }
        }
        this.matches = matchedUsers;
    }
    public void listenForMatches(){
        DatabaseReference userRef = database.getReference(currentUser.getUid());


        userRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> userInfo = dataSnapshot.getChildren();

                for (DataSnapshot data : userInfo) {
                    if (data.getKey().equals("AcceptedUsers")) {
                        List<String> acceptedUsers =  (List<String>) dataSnapshot.child("AcceptedUsers").getValue();
                        findUserByID(currentUser.getUid()).setAcceptedUsers(acceptedUsers);

                    } else if (data.getKey().equals("AcceptedMe")) {
                        List<String> acceptedMe =  (List<String>) dataSnapshot.child("AcceptedMe").getValue();
                        findUserByID(currentUser.getUid()).setAcceptedMe(acceptedMe);

                    } else if (data.getKey().equals("DeclinedUsers")) {
                        List<String> declinedUsers =  (List<String>) dataSnapshot.child("DeclinedUsers").getValue();
                        findUserByID(currentUser.getUid()).setDeclinedUsers(declinedUsers);
                    }
                }

                getMatches();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}