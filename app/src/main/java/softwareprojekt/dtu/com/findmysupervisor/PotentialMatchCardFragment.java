package softwareprojekt.dtu.com.findmysupervisor;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.nex3z.flowlayout.FlowLayout;

import java.util.List;

public class PotentialMatchCardFragment extends Fragment {

    public interface OnCardInteractionListener {
        void onShowMorePressed();
        void onAcceptFling();
        void onDeclineFling();
    }

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    FirebaseStorage storage = FirebaseStorage.getInstance();
    StorageReference storageRef = storage.getReference();

    private OnCardInteractionListener mListener;
    private static String POTENTIAL_MATCH_USER = "";

    private GestureDetector mGestureDetector;

    TextView name;
    TextView studyProgramme;
    TextView bio;
    TextView projectPeriod;
    FlowLayout courses;
    FlowLayout tags;
    ImageView ivPicture;

    FlowBubbleManager coursesManager;
    FlowBubbleManager tagsManager;

    byte[] downloadedImageBytes;


    public static final String TAG = "PotentialMatchCard";

    public PotentialMatchCardFragment() {
        // Required empty public constructor
    }


    public static PotentialMatchCardFragment newInstance(String potentialMatchUser) {
        PotentialMatchCardFragment fragment = new PotentialMatchCardFragment();
        POTENTIAL_MATCH_USER = potentialMatchUser;
        return fragment;
    }

    public void setOnCardInteractionListener(OnCardInteractionListener listener) {
        this.mListener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View mainView = inflater.inflate(R.layout.fragment_potential_card, container,false);

        name = mainView.findViewById(R.id.tvStudentName);
        studyProgramme = mainView.findViewById(R.id.tvStudentProgramme);
        bio = mainView.findViewById(R.id.tvStudentBio);
        projectPeriod = mainView.findViewById(R.id.tvStudentPeriod);
        courses = mainView.findViewById(R.id.flPotentialCardHighlightedCourses);
        tags = mainView.findViewById(R.id.flPotentialCardProjectTags);

        ivPicture = mainView.findViewById(R.id.ivStudentPicture);

        coursesManager = new FlowBubbleManager(getActivity(), courses);
        coursesManager.setHasUserInteractions(false);

        tagsManager = new FlowBubbleManager(getActivity(), tags);
        tagsManager.setHasUserInteractions(false);

        LinearLayout llShowMore = mainView.findViewById(R.id.llShowMore);
        llShowMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onShowMorePressed();
                }
            }
        });
        DatabaseReference potentialMatch = database.getReference(POTENTIAL_MATCH_USER);
        Log.i("DATASNAPSHOT UID", POTENTIAL_MATCH_USER);

        potentialMatch.addListenerForSingleValueEvent(new ValueEventListener() {
              @Override
              public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                  setCardData(dataSnapshot);
              }

              @Override
              public void onCancelled(@NonNull DatabaseError databaseError) {

              }
          });

        CardView cvPotentialMatchCard = mainView.findViewById(R.id.cvMatchCard);
        mGestureDetector = new GestureDetector(getActivity(),
            new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onFling(MotionEvent e1, MotionEvent e2,
                                       float velocityX, float velocityY) {
                    if (velocityX < -100.0f && (Math.abs(velocityX) > Math.abs(velocityY))) {
                        Log.i(TAG, "decline");
                        if (mListener != null) {
                            mListener.onDeclineFling();
                        }
                    } else if (velocityX > 100.0f && (Math.abs(velocityX) > Math.abs(velocityY))) {
                        Log.i(TAG, "accept");
                        if (mListener != null) {
                            mListener.onAcceptFling();
                        }
                    } else {
                        Log.i(TAG, "bad fling");
                    }

                    return true;
                }
            });

        cvPotentialMatchCard.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                mGestureDetector.onTouchEvent(motionEvent);
                return true;
            }
        });


        Log.i("DATASNAPSHOT UID", POTENTIAL_MATCH_USER);

        return mainView;
    }

    // Updates the card with fetched data
    public void setCardData(DataSnapshot dataSnapshot) {


        Iterable<DataSnapshot> dataEntries = dataSnapshot.getChildren();

        StorageReference pictureRef = storageRef.child("images/" + POTENTIAL_MATCH_USER);
        pictureRef.getBytes(Long.MAX_VALUE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {

                downloadedImageBytes = bytes;

                Bitmap image = BitmapFactory.decodeByteArray(downloadedImageBytes,0, bytes.length);
                if(ivPicture != null) {
                    ivPicture.setImageBitmap(image);
                }
            }
        });

        for (DataSnapshot data: dataEntries){

            if(data.getKey().equals("Name")){
                this.name.setText(dataSnapshot.child("Name").getValue().toString());
                Log.i("MATCHINGALGO VALUE", dataSnapshot.child("Name").getValue().toString());
            }
            if(data.getKey().equals("Bio")){
                this.bio.setText(dataSnapshot.child("Bio").getValue().toString());
            }
            if(data.getKey().equals("StudyProgramme")){
                this.studyProgramme.setText(dataSnapshot.child("StudyProgramme").getValue().toString());
            }
            if(data.getKey().equals("ProjectPeriod")){
                this.projectPeriod.setText(dataSnapshot.child("ProjectPeriod").getValue().toString());
            }
            if(data.getKey().equals("Courses")){
                List<String > stringCourses = (List<String>) dataSnapshot.child("Courses").getValue();
                coursesManager.addAll(stringCourses);
            }
            if(data.getKey().equals("Tags")){
                List<String > stringTags = (List<String>) dataSnapshot.child("Tags").getValue();
                tagsManager.addAll(stringTags);
            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public byte[] getDownloadedImageBytes() {
        return downloadedImageBytes;
    }

}
