package softwareprojekt.dtu.com.findmysupervisor;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nex3z.flowlayout.FlowLayout;

import java.util.ArrayList;

public class PotentialDetailedFragment extends Fragment {

    public interface DetailedListener {
        void onDetailDismissPressed();
        void onDetailAcceptPressed();
        void onDetailDeclinePressed();
    }

    static DetailedListener mListener;

    public PotentialDetailedFragment () {

    }

    public static final String BUTTON_BOOL = "showButtons";

    public static PotentialDetailedFragment newInstance(User user, byte[] imageBytes, DetailedListener listener, boolean shouldDisplayButtons) {
        PotentialDetailedFragment fragment = new PotentialDetailedFragment();
        Bundle args = new Bundle();
        args.putString(User.NODE_NAME, user.name);
        args.putString(User.NODE_BIO, user.bio);
        args.putString(User.NODE_STUDY_PROGRAMME, user.studyProgramme);
        args.putString(User.NODE_PROJECT_PERIOD, user.period);
        args.putStringArrayList(User.NODE_COURSES, new ArrayList<String>(user.courses));
        args.putStringArrayList(User.NODE_TAGS, new ArrayList<String>(user.tags));
        args.putByteArray(User.NODE_IMAGE, imageBytes);
        args.putBoolean(BUTTON_BOOL,shouldDisplayButtons);
        fragment.setArguments(args);

        mListener = listener;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View modalView = inflater.inflate(R.layout.fragment_potential_detailed,container, false);
        Toolbar toolbar = modalView.findViewById(R.id.tbPotentialDetailed);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    mListener.onDetailDismissPressed();
                }
            }
        });

        TextView name = modalView.findViewById(R.id.tvStudentName);
        name.setText(getArguments().getString(User.NODE_NAME));

        TextView studyProgramme = modalView.findViewById(R.id.tvStudentProgramme);
        studyProgramme.setText(getArguments().getString(User.NODE_STUDY_PROGRAMME));

        TextView bio = modalView.findViewById(R.id.tvStudentBio);
        bio.setText(getArguments().getString(User.NODE_BIO));

        TextView projectPeriod = modalView.findViewById(R.id.tvStudentPeriod);
        projectPeriod.setText(getArguments().getString(User.NODE_PROJECT_PERIOD));

        FlowLayout courses = modalView.findViewById(R.id.flPotentialCardHighlightedCourses);
        FlowBubbleManager courseManager = new FlowBubbleManager(getActivity(), courses);
        courseManager.setHasUserInteractions(false);
        courseManager.addAll(getArguments().getStringArrayList(User.NODE_COURSES));

        FlowLayout tags = modalView.findViewById(R.id.flPotentialCardProjectTags);
        FlowBubbleManager tagsManager = new FlowBubbleManager(getActivity(), tags);
        tagsManager.setHasUserInteractions(false);
        tagsManager.addAll(getArguments().getStringArrayList(User.NODE_TAGS));

        FloatingActionButton decline = modalView.findViewById(R.id.btnNope);
        decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    mListener.onDetailDeclinePressed();
                }
            }
        });

        FloatingActionButton accept = modalView.findViewById(R.id.btnLike);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    mListener.onDetailAcceptPressed();
                }
            }
        });

        if(!getArguments().getBoolean(BUTTON_BOOL)){
            accept.hide();
            decline.hide();
        }

        byte[] bytes = getArguments().getByteArray(User.NODE_IMAGE);
        if (bytes != null) {
            Bitmap image = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
            ImageView imageView = modalView.findViewById(R.id.ivStudentPicture);
            imageView.setImageBitmap(image);
        }

        return modalView;
    }

}