package softwareprojekt.dtu.com.findmysupervisor;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class MainViewFragment extends Fragment implements
        MatchButtonsFragment.OnMatchButtonsListener,
        PotentialMatchCardFragment.OnCardInteractionListener,
        NoMorePotentialMatchesFragment.NoMorePotentialMatchesListener,
        Matching.MatchingListener,
        PotentialDetailedFragment.DetailedListener{

    public static final String TAG = "MainViewFragment";

    FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
    FirebaseDatabase database = FirebaseDatabase.getInstance();

    static Matching matching;

    private static String CURRENT_SHOWN_USER = "";

    PotentialMatchCardFragment mPotentialMatchCardFragment;
    MatchButtonsFragment mMatchButtonsFragment;
    PotentialDetailedFragment mPotentialDetailedFragment;

    public MainViewFragment() {
        // Required empty public constructor
    }


    public static MainViewFragment newInstance(Matching currentMatching) {
        MainViewFragment fragment = new MainViewFragment();
        Bundle args = new Bundle();
        matching = currentMatching;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View mainView = inflater.inflate(R.layout.fragment_main, container,false);

        String nextUID = getNextUID();

        if (nextUID != null) {

            mMatchButtonsFragment = MatchButtonsFragment.newInstance();
            mMatchButtonsFragment.setOnMatchButtonsListener(this);

            mPotentialMatchCardFragment = getNewUserCard(nextUID);

            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.add(R.id.flPotentialMatchCardContainer, mPotentialMatchCardFragment);
            ft.add(R.id.flPotentialMatchButtonsContainer, mMatchButtonsFragment,"buttons");
            ft.commit();
        } else {

            NoMorePotentialMatchesFragment fragment = NoMorePotentialMatchesFragment.newInstance();
            fragment.setOnNoMorePotentialMatchesListener(this);

            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.flPotentialMatchCardContainer, fragment);
            ft.commit();
        }

        Matching.setOnNewUsersFetchedListener(this);

        return mainView;
    }

    public PotentialMatchCardFragment getNewUserCard(String UID){

        CURRENT_SHOWN_USER = UID;
        PotentialMatchCardFragment cardFrag = PotentialMatchCardFragment.newInstance(UID);
        cardFrag.setOnCardInteractionListener(this);

        return cardFrag;

    }

    public String getNextUID() {
        if(matching.sortedUsers.isEmpty()){
            //Log.i("MATCHINGALGO ISEMPTY:", "No More Users");
            return null;
        }
        else{
            //Log.i("MATCHINGALGO NEXTUID:", Matching.sortedUsers.get(0).UID);
            //Log.i("MATCHINGALGO SIZE:", Integer.toString(Matching.sortedUsers.size()));
            return matching.sortedUsers.get(0).UID;
        }

    }

    public void replaceCard(boolean didLike) {

        Log.i(TAG, "Replace card called");

        int outTransition = (didLike) ? R.animator.slide_out_right : R.animator.slide_out_left;

        String nextUID = getNextUID();
        if (nextUID != null) {

            FragmentTransaction ft = getFragmentManager().beginTransaction();

            if (mMatchButtonsFragment == null) {
                mMatchButtonsFragment = MatchButtonsFragment.newInstance();
                mMatchButtonsFragment.setOnMatchButtonsListener(this);
                ft.add(R.id.flPotentialMatchButtonsContainer, mMatchButtonsFragment,"buttons");
            }

            mPotentialMatchCardFragment = getNewUserCard(nextUID);

            ft.setCustomAnimations(R.animator.slide_in_from_top, outTransition);
            ft.replace(R.id.flPotentialMatchCardContainer, mPotentialMatchCardFragment);

            ft.commit();

        } else {

            NoMorePotentialMatchesFragment fragment = NoMorePotentialMatchesFragment.newInstance();
            fragment.setOnNoMorePotentialMatchesListener(this);

            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.animator.slide_in_from_top, outTransition);
            ft.replace(R.id.flPotentialMatchCardContainer, fragment);
            if (mMatchButtonsFragment != null) {
                ft.remove(mMatchButtonsFragment);
            }

            ft.commit();
            mPotentialMatchCardFragment = null;
            mMatchButtonsFragment = null;
        }
    }

    public void accept() {
        matching.sortedUsers.remove(0);
        User currentUser  = matching.findUserByID(FirebaseAuth.getInstance().getCurrentUser().getUid());
        User shownUser = matching.findUserByID(CURRENT_SHOWN_USER);
        currentUser.acceptedUsers.add(CURRENT_SHOWN_USER);
        shownUser.acceptedMe.add(FirebaseAuth.getInstance().getCurrentUser().getUid());
        DatabaseReference cUserRef = database.getReference(this.currentUser.getUid());
        DatabaseReference sUserRef = database.getReference(CURRENT_SHOWN_USER);
        List<String> acceptedUIDs = new ArrayList<>();
        for (String user: currentUser.acceptedUsers) {
            acceptedUIDs.add(user);
        }
        List<String> acceptedMeUIDs = new ArrayList<>();
        for (String user: shownUser.acceptedMe) {
            acceptedMeUIDs.add(user);
        }

        cUserRef.child("AcceptedUsers").setValue(acceptedUIDs);
        sUserRef.child("AcceptedMe").setValue(acceptedMeUIDs);

        replaceCard(true);
    }

    public void decline() {
        matching.sortedUsers.remove(0);
        User currentUser  = matching.findUserByID(FirebaseAuth.getInstance().getCurrentUser().getUid());
        currentUser.declinedUsers.add(CURRENT_SHOWN_USER);
        DatabaseReference cUserRef = database.getReference(FirebaseAuth.getInstance().getCurrentUser().getUid());
        List<String> declinedUIDs = new ArrayList<>();
        for (String user: currentUser.declinedUsers) {
            declinedUIDs.add(user);
        }
        cUserRef.child("DeclinedUsers").setValue(declinedUIDs);

        replaceCard(false);
    }

    @Override
    public void onAcceptPressed() {
        accept();
    }

    @Override
    public void onDeclinePressed() {
        decline();
    }

    @Override
    public void onShowMorePressed() {

        mPotentialDetailedFragment = PotentialDetailedFragment.newInstance(matching.sortedUsers.get(0), mPotentialMatchCardFragment.getDownloadedImageBytes(), this,true);
        ((MainActivity) getActivity()).showDetailedModal(mPotentialDetailedFragment);
    }

    @Override
    public void onAcceptFling() {
        accept();
    }

    @Override
    public void onDeclineFling() {
        decline();
    }

    @Override
    public void onRefreshPressed() {

        matching.getUsers();

    }

    /*
        From Matching class
     */

    @Override
    public void onNewUsersFetched() {
        Log.i(TAG, "New users fetched" + matching.sortedUsers.size());
        if (getNextUID() != null && mPotentialMatchCardFragment == null) {
            Log.i(TAG, "replace card");
            replaceCard(true);
        }
    }

    /*
        Detailed Listeners
     */
    @Override
    public void onDetailDismissPressed() {
        ((MainActivity) getActivity()).dismissDetailedModal(mPotentialDetailedFragment);
        mPotentialDetailedFragment = null;
    }

    @Override
    public void onDetailAcceptPressed() {
        ((MainActivity) getActivity()).dismissDetailedModal(mPotentialDetailedFragment);
        mPotentialDetailedFragment = null;
        accept();
    }

    @Override
    public void onDetailDeclinePressed() {
        ((MainActivity) getActivity()).dismissDetailedModal(mPotentialDetailedFragment);
        mPotentialDetailedFragment = null;
        decline();
    }
}
