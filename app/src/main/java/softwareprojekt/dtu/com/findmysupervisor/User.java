package softwareprojekt.dtu.com.findmysupervisor;

import java.util.ArrayList;
import java.util.List;

public class User {

    public static final String NODE_NAME = "Name";
    public static final String NODE_STUDY_PROGRAMME = "StudyProgramme";
    public static final String NODE_PROJECT_PERIOD = "ProjectPeriod";
    public static final String NODE_BIO = "Bio";
    public static final String NODE_COURSES = "Courses";
    public static final String NODE_TAGS = "Tags";
    public static final String NODE_IMAGE = "Image";

    String UID;
    String name;
    String bio;
    String studyProgramme;
    String period;
    List<String> courses;
    List<String> tags;
    String email;

    List<String> acceptedUsers = new ArrayList<>();
    List<String> declinedUsers = new ArrayList<>();

    List<String> acceptedMe = new ArrayList<>();

    public User(String UID, String name, String bio, String studyProgramme, String period, List<String> courses, List<String> tags, String email){
        this.UID = UID;
        this.name = name;
        this.bio = bio;
        this.studyProgramme=studyProgramme;
        this.period=period;
        this.courses=courses;
        this.tags=tags;
        this.email = email;

    }
    public User(){

    }

    public void setAcceptedUsers(List<String> UIDs){
        this.acceptedUsers = UIDs;
    }
    public void setAcceptedMe (List<String> UIDs){
        this.acceptedMe = UIDs;
    }
    public void setDeclinedUsers(List<String> UIDs){
        this.declinedUsers = UIDs;
    }

    public boolean inList(List<User> userList){
        for(User u : userList){
            if (u.UID.equals(this.UID)){
                return true;
            }
        }
        return false;
    }
}
