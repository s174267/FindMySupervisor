package softwareprojekt.dtu.com.findmysupervisor;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class NoMorePotentialMatchesFragment extends Fragment {

    public interface NoMorePotentialMatchesListener {
        void onRefreshPressed();
    }

    NoMorePotentialMatchesListener mListener;

    public static NoMorePotentialMatchesFragment newInstance() {
        NoMorePotentialMatchesFragment fragment = new NoMorePotentialMatchesFragment();
        return fragment;
    }

    public void setOnNoMorePotentialMatchesListener(NoMorePotentialMatchesListener listener) {
        mListener = listener;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_no_more_potential_matches, container,false);

        Button bRefresh = view.findViewById(R.id.bNoMoreRefresh);
        bRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    mListener.onRefreshPressed();

                }
            }
        });

        return view;
    }
}
