package softwareprojekt.dtu.com.findmysupervisor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.nex3z.flowlayout.FlowLayout;

import java.util.ArrayList;
import java.util.List;

public class FlowBubbleManager implements View.OnLongClickListener, PopupMenu.OnMenuItemClickListener {

    Context mContext;
    FlowLayout mLayout;
    ArrayList<TextView> mTextViews;

    TextView currentLongClickedTextView = null;

    boolean hasUserInteractions = true;

    public FlowBubbleManager(Context context, FlowLayout layout) {
        this.mContext = context;
        this.mLayout = layout;
        mTextViews = new ArrayList<>();
    }

    public void setHasUserInteractions (boolean userInteractions) {
        this.hasUserInteractions = userInteractions;
    }

    public void addAll(List<String> elements) {
        for (String element: elements) {
            add(element);
        }
    }

    public void add(String element) {
        element = element.toLowerCase();
        // Determine if tag already exists
        boolean tagAlreadyExists = false;
        for (TextView text : mTextViews) {
            tagAlreadyExists = text.getText().equals(element) || tagAlreadyExists;
        }

        if (!tagAlreadyExists) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            TextView textView = (TextView) inflater.inflate(R.layout.bubble_view, null, false);
            textView.setText(element);

            if (hasUserInteractions) {
                textView.setOnLongClickListener(this);
            }

            mLayout.addView(textView);
            mTextViews.add(textView);
        } else {
            Toast.makeText(mContext, mContext.getResources().getString(R.string.tag_already_exists),Toast.LENGTH_SHORT).show();
        }

    }

    public ArrayList<String> getElements() {
        ArrayList<String> elements = new ArrayList<String>();
        for (int i = 0; i < mTextViews.size(); i++) {
            elements.add(mTextViews.get(i).getText().toString());
        }
        return elements;
    }

    public void removeByView(TextView textView) {
        if (mTextViews.contains(textView)) {
            mLayout.removeView(textView);
            mTextViews.remove(textView);
        }
    }


    @Override
    public boolean onLongClick(View view) {
        currentLongClickedTextView = (TextView) view;

        PopupMenu popup = new PopupMenu(mContext, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_bubble, popup.getMenu());
        popup.setOnMenuItemClickListener(this);
        popup.show();
        return true;
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        if (menuItem.getItemId() == R.id.miBubbleDelete) {
            removeByView(currentLongClickedTextView);
            currentLongClickedTextView = null;
            return true;
        }
        return false;
    }
}
