package softwareprojekt.dtu.com.findmysupervisor;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.ByteArrayOutputStream;

public class MatchesViewFragment extends Fragment{

    ListView mListView;

    PotentialDetailedFragment mPotentialDetailedFragment;
    FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    FirebaseStorage storage = FirebaseStorage.getInstance();

    DatabaseReference userRefAcceptedMe = database.getReference(currentUser.getUid()).child("AcceptedMe");
    DatabaseReference userRefAcceptedUsers = database.getReference(currentUser.getUid()).child("AcceptedUsers");

    StorageReference storageRef = storage.getReference();
    PotentialDetailedFragment.DetailedListener detailedListener;

    static Matching matching;

    public static MatchesViewFragment newInstance(Matching currentMatching) {
        MatchesViewFragment fragment = new MatchesViewFragment();
        matching = currentMatching;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        detailedListener = new PotentialDetailedFragment.DetailedListener() {

            @Override
            public void onDetailDismissPressed() {
                ((MainActivity) getActivity()).dismissDetailedModal(mPotentialDetailedFragment);
                mPotentialDetailedFragment = null;
            }

            @Override
            public void onDetailAcceptPressed() {
                ((MainActivity) getActivity()).dismissDetailedModal(mPotentialDetailedFragment);
                mPotentialDetailedFragment = null;
            }

            @Override
            public void onDetailDeclinePressed() {
                ((MainActivity) getActivity()).dismissDetailedModal(mPotentialDetailedFragment);
                mPotentialDetailedFragment = null;
            }

        };

        final View matchesView = inflater.inflate(R.layout.fragment_matches, container,false);
        mListView = matchesView.findViewById(R.id.lvMatches);

        final CustomAdapter customAdapter = new CustomAdapter();
        mListView.setAdapter(customAdapter);

        mListView.setFooterDividersEnabled(true);
        TextView footerView = (TextView) getLayoutInflater().inflate(R.layout.footer_view_matches, null);
        mListView.addFooterView(footerView,null, false);

        userRefAcceptedMe.limitToLast(1).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                customAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        userRefAcceptedUsers.limitToLast(1).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                customAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        return matchesView;
    }

    class CustomAdapter extends BaseAdapter{


        @Override
        public int getCount() {
            return matching.matches.size();
        }

        @Override
        public Object getItem(int pos) {
            return null;
        }
        @Override
        public long getItemId(int pos) {
            return 0;
        }
        public View getView(final int position, View convertView, ViewGroup parent) {

            View view = getActivity().getLayoutInflater().inflate(R.layout.matches_list_item, null);


            final ImageView mImageView = view.findViewById(R.id.ivMatchPicture);
            TextView mTextViewName = view.findViewById(R.id.tvMatchName);
            TextView mTextViewProgramme = view.findViewById(R.id.tvMatchStudyProgramme);
            final ImageButton mImageButton = view.findViewById(R.id.imSendEmail);

            mTextViewName.setText(matching.findUserByID(matching.matches.get(position)).name);
            mTextViewProgramme.setText(matching.findUserByID(matching.matches.get(position)).studyProgramme);
            StorageReference pictureRef = storageRef.child("images/" + matching.findUserByID(matching.matches.get(position)).UID);
            pictureRef.getBytes(Long.MAX_VALUE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                @Override
                public void onSuccess(byte[] bytes) {
                    Bitmap bitmap = BitmapFactory.decodeByteArray(bytes,0, bytes.length);
                    if(mImageView != null) {
                        mImageView.setImageBitmap(bitmap);
                    }
                }
            });

            final String uri = "mailto:" + matching.findUserByID(matching.matches.get(position)).email + "?subject=Meeting?";

            mImageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);

                    Uri data = Uri.parse(uri);
                    intent.setData(data);
                    startActivity(intent);
                }
            });

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bitmap bitmap = ((BitmapDrawable) mImageView.getDrawable()).getBitmap();
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    byte[] imageInByte = baos.toByteArray();
                    User clickedUser = matching.findUserByID(matching.matches.get(position));
                    mPotentialDetailedFragment = PotentialDetailedFragment.newInstance(clickedUser, imageInByte,detailedListener,false);
                    ((MainActivity) getActivity()).showDetailedModal(mPotentialDetailedFragment);
                }
            });
            return view;

        }

    }

}