Softwareprojekt 2018 - Android App Development

AUTHORS:
Group 4, StudyBuddy/Supervisr
Felix Larsen, s174296
Tobias Jessen, s174292
Niels Mikkelsen, s174290
Lasse Pedersen, s174253
Victor Foged, s174267


INTRODUCTION:
This app is created as part of the mandatory assignment in the course "02128 Softwareprojekt 2018" at DTU Compute.


INSTALLATION:
After unzipping the folder. Import the folder as a project within Android Studio or Eclipse (note: This is developed using Android Studio).


PROGRAM EXECUTION:
You are required to either having a emulator installed, or an actual Android device running running at minimum API level 23.
When running the app you are greeted by the login screen.


CREATING A USER:
In order to get pass the login screen you need a user. Input an email-address and a password and then press "Create account". The requirements to the
email-address and password are as follows:

E-mail address' are required to contain a "@"-icon. You are allowed to use a dummy-mail account, since we do not send any confirmation mails.
Please note that if you wish to send a mail, through the app, to a user that you have matched with you are sent to your default email-app with the
subsequent default sender-address.

Passwords are required to have at least 6 characters.


ALREADY CREATED USER:
If you do not wish to create a new user, you can use the following test person:
Email:      hcorsted@gmail.com
Password:   password
